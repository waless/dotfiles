
# ------------------- begin color-setting ----------------------------
export TERM=xterm-256color

# prompt color
case ${UID} in
  # root
  0)
  PROMPT="%{$fg_bold[cyan]%}%m%{$fg_bold[red]%}#%{$reset_color%} "
  PROMPT2="%{$fg[magenta]%}%_%{$reset_color%}%{$fg_bold[white]%}>>%{$reset_color%} "
  ;;
  # other
  *)
  PROMPT="%{$fg_bold[green]%}%m%{$fg_bold[white]%}%%%{$reset_color%} "
  PROMPT2="%{$fg[magenta]%}%_%{$reset_color%}%{$fg_bold[white]%}>>%{$reset_color%} "
  ;;
esac

# command color
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -hF --color=auto'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
# ------------------- end color-setting ----------------------------

# apt-cyg setting
#alias apt-cyg='apt-cyg -m ftp://ftp.jaist.ac.jp/pub/cygwin/ -c /package'

# vim path
#export VIM=~/install/share/vim
#export VIMRUNTIME=$VIM/vim73

# user bin path
export PATH=/usr/local/bin:$PATH
export PATH=~/install/bin:$PATH
export PATH=/c/Ruby/Ruby200/bin:$PATH
export PATH=/c/Python/Python27:$PATH

# default editor
export EDITOR=/bin/vim

# language setting
export LANG='ja_JP.UTF-8'
export LC_ALL='ja_JP.UTF-8'
export LC_MESSAGES='ja_JP.UTF-8'

# rbenv setting
#if [ -e $HOME/.rbenv ] ; then
#    export PATH=~/.rbenv/bin:$PATH
#    eval "$(rbenv init -)"
#fi

# python virtualenv setting
#if [ -e /bin/virtualenvwrapper.sh ] ; then
#    export WORKON_HOME=$HOME/.virtualenvs
#    source virtualenvwrapper.sh
#fi

# shell machine local setting
if [ -f $HOME/.zshrc.local ] ; then
    source $HOME/.zshrc.local
fi

