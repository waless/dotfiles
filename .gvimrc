
colorscheme lucius

set visualbell t_vb=
autocmd guienter * FullScreen

set guioptions-=T
set guioptions-=m

"背景暗く
set background=dark

" win32用設定
if has('win32')
    " フォント
    set encoding=cp932
    set guifont=Myrica\ M:h12
    set guifontwide=Myrica\ M:h12
    set ambiwidth=double
    set encoding=utf-8
endif

