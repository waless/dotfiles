﻿" windowsでも.vimフォルダを使えるように
if has('vim_starting') && has('win32')
    set runtimepath+=~/.vim,~/.vim/after
endif

" これをencodingの前に書かないとターミナルが文字化けする
let &termencoding=&encoding
" 文字コード優先順位
set fileencodings=utf-8,cp932,euc-jp
" 内部文字コード
set encoding=utf-8

" vim-bundle
filetype off
filetype indent off

" terminal-cursor switch normal/insert
if has('win32unix')
    let &t_ti.="\eP\e[1 q\e\\"
    let &t_SI.="\eP\e[5 q\e\\"
    let &t_EI.="\eP\e[1 q\e\\"
    let &t_te.="\eP\e[0 q\e\\"

    let &t_SI.="\eP\e[<r\e\\"
    let &t_EI.="\eP\e[<s\e[<0t\e\\"
    let &t_te.="\eP\e[<0t\e[<s\e\\"
elseif has('mac')
    let &t_SI="\eP\<ESC>]50;CursorShape=1\x7\e\\"
    let &t_EI="\eP\<ESC>]50;CursorShape=0\x7\e\\"
    inoremap <ESC> <ESC>gg`]
endif

if has('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim/
    set runtimepath+=$GOPATH/misc/vim
endif

call neobundle#begin(expand('$HOME/.vim/bundle/'))

" プラグインの配置
NeoBundle 'h1mesuke/vim-alignta'
NeoBundle 'kana/vim-altr'
NeoBundle 'kana/vim-tabpagecd'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'Shougo/neocomplete'
NeoBundle 'Shougo/neosnippet'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'Shougo/neomru.vim'
NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/vimfiler'
NeoBundle 'thinca/vim-quickrun'
NeoBundle 'tpope/vim-surround'
NeoBundle 'gtags.vim'
NeoBundle 'Lucius'
NeoBundle 'vcscommand.vim'
NeoBundle 'vim-jp/vim-go-extra'

call neobundle#end()

filetype plugin on
filetype indent on
filetype on

" 改行コードの自動認識
set fileformats=unix

" 行番号の表示
set number

" 行番号を相対的に表示
"set relativenumber

" カーソル位置をハイライト
set cursorline
set cursorcolumn

" 右下にファイル情報の表示
set ruler

" ステータスラインを常に表示
set laststatus=2
set statusline=
set statusline+=%=\ %m%r%h%w%y%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}\ %l,%c%V%8P

" 行の最大幅。0で自動改行を行わない
set textwidth=0

" バックアップは不要
set nobackup

" アンドゥファイルは不要
set noundofile

" スワップディレクトリ設定
set directory=$HOME/.vim/tmp/swap

" vi互換を行わない
set nocompatible

" tabをスペースに変換する
set expandtab

" バッファを切り替えても保持する
set hidden

" インデントサイズ倍数
set shiftwidth=4

" インデントサイズ
set tabstop=4

" tab入力時にshitwidthを使う
set smarttab

" 自動インデント有効
set autoindent

" C言語の高度なインデントを使用
set smartindent

" インクリメンタル検索を使用する
set incsearch

" 末尾まで検索したら最初から検索する
set wrapscan

" 検索文字列をハイライトする
set hlsearch

" 検索時、大文字小文字を区別しない
set ignorecase

" 検索時、大文字が含まれていれば大文字小文字を区別する
set smartcase

" ウィンドウの幅を超えても折り返さない
set nowrap

" 行を超えて機能するキーを設定
set whichwrap=b,s,h,l,<,>,[,]

" 外部でファイルが変更された時、自動で読み込む
set autoread

" indend :行頭の空白の削除を許可 
" eol    :改行の削除を許可
" start  :挿入モードの開始位置でも削除を許可
set backspace=indent,eol,start

" tagファイルをディレクトリを遡るように
if has('path_extra')
    set tags=./tags;
endif

" vimgrepで自動でQuickFixを開くように
autocmd QuickFixCmdPost make,grep,grepadd,vimgrep if len(getqflist()) != 0 | copen | endif

" 外部grepはパスが通っているものを使用
set grepprg=grep\ -nH

" 256colorが使用できるならsyntaxとcolorscheme有効
if &t_Co >= 256 || has('gui_starting')
    syntax on
    colorscheme lucius
endif

autocmd FileType go autocmd BufWritePre <buffer> Fmt
exe "set rtp+=".globpath($GOPATH, "src/github.com/nsf/gocode/vim")
set completeopt=menu,preview

" neocomplete
" 
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#smart_close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
" Or set this.
"let g:neocomplete#enable_cursor_hold_i = 1
" Or set this.
"let g:neocomplete#enable_insert_char_pre = 1

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'


" neosnippet
imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)
xmap <C-k> <Plug>(neosnippet_expand_target)

imap <expr><TAB>
            \ neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)"
            \ : pumvisible() ? "\<C-n>" : "\<TAB>"

smap <expr><TAB>
            \ neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)"
            \ : "\<TAB>"

if has('conceal')
    set conceallevel=2 concealcursor=niv
endif

let g:neosnippet#enable_snipmate_compatibility=1

let g:neosnippet#snippets_directory='$HOME/.vim/snippets'

" unite
" uniteプレフィックスキー設定
nnoremap [unite] <NOP>
nmap <Space>u [unite]

" unite系が使用するファイル置き場の指定
let g:unite_data_directory='~/.vim/tmp/unite'

" インサートモードで開始
let g:unite_enable_start_insert=1

" 保存する履歴の最大数
let g:unite_source_file_mru_limit=50

" file_rec時の最大キャッシュ数
let g:unite_source_file_rec_max_cache_files=10000

" file_recの無視ファイル
call unite#custom#source('file_rec,file_rec/async', 'ignore_pattern', '\(\.obj$\|\.o$\|\.d$\|\.tlog$\|\.pdb$\|\.log$\|\.raw$\|\.bmp$\|\.png$\|\.jpg$\|\.jpeg$\|\.gif$\|\.wav$\|\.mp3$\|\.ogg$\|\.oga$\|\.zip$\|\.gzip$\|\.gz$\|\.bz2$\|\.lzma$\|\.usm$\|\.flb$\)')

"call unite#set_profile('default', 'ignorecase', 1)
"call unite#set_profile('default', 'smartcase', 1)

" ptが使用できるなら置き換えられるものを置き換え
if executable('pt')
    let g:unite_source_grep_command='pt'
    let g:unite_source_grep_default_opts='--nogroup --nocolor --column'
    let g:unite_source_grep_recursive_opt=''

    let g:unite_source_rec_async_command='pt --follow --nogroup --hidden -g ""'
endif

" ショートカットキーバインド
nnoremap <silent> [unite]b :<C-u>Unite buffer<CR>
nnoremap <silent> [unite]f :<C-u>UniteWithBufferDir -buffer-name=files file<CR>
nnoremap <silent> [unite]r :<C-u>Unite -buffer-name=register register<CR>
nnoremap <silent> [unite]m :<C-u>Unite file_mru<CR>
nnoremap <silent> [unite]s :<C-u>Unite source<CR>
nnoremap <silent> [unite]u :<C-u>Unite buffer file_mru<CR>
nnoremap <silent> [unite]a :<C-u>UniteWithBufferDir -buffer-name=files buffer file_mru bookmark file<CR>
nnoremap <silent> [unite]p :<C-u>Unite file_rec/async<CR>
nnoremap <silent> [unite]g :<C-u>Unite grep<CR>

au FileType unite nnoremap <silent> <buffer> <expr> <C-s> unite#do_action('split')
au FileType unite inoremap <silent> <buffer> <expr> <C-s> unite#do_action('split') 
au FileType unite nnoremap <silent> <buffer> <expr> <C-v> unite#do_action('vsplit')
au FileType unite inoremap <silent> <buffer> <expr> <C-v> unite#do_action('vsplit')

au FileType unite nnoremap <silent> <buffer> <ESC><ESC> :q<CR>
au FileType unite inoremap <silent> <buffer> <ESC><ESC> <ESC>:q<CR>

" unite-outline
nnoremap <silent> [unite]o :<C-u>Unite outline<CR>

" unite-tag
nnoremap <silent> [unite]t  :<C-u>Unite tag<CR>
nnoremap <silent> [unite]ti :<C-u>Unite tag/include<CR>
nnoremap <silent> [unite]tf :<C-u>Unite tag/file<CR>

" vim-filer
nnoremap <silent> vf : VimFiler -buffer-name=explorer -split -winwidth=45 -toggle -no-quit<CR>
let g:vimfiler_safe_mode_by_default=0

" vim-ref
if has('vim_starting') && has('win32')
    let $PATH=$PATH . ';C:/Program Files/Lynx for Win32'
endif
let g:ref_alc_encoding='euc-jp'

" syntastic
let g:syntastic_mode_map={'mode':'passive', 'active_filetypes': ['go'] }
let g:syntastic_go_checkers=['go', 'golint']

" 環境別の設定ファイル
if exists('$HOME/.vimrc.local')
    source $HOME/.vimrc.local
endif

" cscope
" 環境別に作業ディレクトリへ移動する可能性があるため、データベースを追加するため
" 環境別設定の後に設定する
if has('cscope')
    if has('win32cygwin')
        set csprg=mlcscope
    else
        set csprg=cscope
    endif

    set csto=0
    set cst
    set nocsverb
    " add any database in current directory
    if filereadable('cscope.out')
        cs add cscope.out .
    " else add database pointed to by environment
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif
    set csverb
    set cscopequickfix=s-,c-,d-,i-,t-,e-
endif

"エラー音を出ないように
set visualbell t_vb=

