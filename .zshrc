
# ------------------- begin color-setting ----------------------------
export TERM=xterm-256color

# enable prompt color
autoload -Uz colors
colors
setopt prompt_subst

# prompt color
case ${UID} in
  # root
  0)
  PROMPT="%{$fg_bold[cyan]%}%m%{$fg_bold[red]%}#%{$reset_color%} "
  PROMPT2="%{$fg[magenta]%}%_%{$reset_color%}%{$fg_bold[white]%}>>%{$reset_color%} "
  ;;
  # other
  *)
  PROMPT="%{$fg_bold[green]%}%m%{$fg_bold[white]%}%%%{$reset_color%} "
  PROMPT2="%{$fg[magenta]%}%_%{$reset_color%}%{$fg_bold[white]%}>>%{$reset_color%} "
  ;;
esac

# current line mark
#RPROMPT="%{$fg_bold[white]%}[%{$reset_color%}%{$fg[cyan]%}%~%{$reset_color%}%{$fg_bold[white]%}]%{$reset_color%}"
#
#SPROMPT="%{$fg_bold[red]%}correct%{$reset_color%}: %R -> %r ? "

# command color
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias ls='ls -hF --color=auto'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

    export ZLS_COLORS=$LS_COLORS
    zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
fi
# ------------------- end color-setting ----------------------------

# ------------------- begin complation setting ---------------------
# init complation
autoload -U compinit
compinit -u

setopt list_types
setopt auto_list
setopt auto_menu
setopt auto_param_keys
setopt list_packed
# ------------------- end complation setting -----------------------

# not play beep
setopt nobeep

# enable spell check
setopt correct

# apt-cyg setting
alias apt-cyg='apt-cyg -m ftp://ftp.jaist.ac.jp/pub/cygwin/ -c /package'

# vim path
#export VIM=~/install/share/vim
#export VIMRUNTIME=$VIM/vim73

# user bin path
export PATH=~/install/bin:$PATH

# default editor
export EDITOR=/bin/vim

# language setting
export LANG='ja_JP.UTF-8'
export LC_ALL='ja_JP.UTF-8'
export LC_MESSAGES='ja_JP.UTF-8'

# rbenv setting
#if [ -e $HOME/.rbenv ] ; then
#    export PATH=~/.rbenv/bin:$PATH
#    eval "$(rbenv init -)"
#fi

# python virtualenv setting
#if [ -e /bin/virtualenvwrapper.sh ] ; then
#    export WORKON_HOME=$HOME/.virtualenvs
#    source virtualenvwrapper.sh
#fi

# shell machine local setting
if [ -f $HOME/.zshrc.local ] ; then
    source $HOME/.zshrc.local
fi

