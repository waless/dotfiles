#! /bin/sh

if [ -x $HOME/dotfiles/.zshrc ]
then
    rm -f $HOME/.zshrc
    ln -s $HOME/dotfiles/.zshrc $HOME/.zshrc
fi

if [ -x $HOME/dotfiles/.bashrc ]
then
    rm -f $HOME/.bashrc
    ln -s $HOME/dotfiles/.bashrc $HOME/.bashrc
fi

if [ -x $HOME/dotfiles/.vimrc ]
then
    rm -f $HOME/.vimrc
    cp $HOME/dotfiles/.vimrc $HOME/.vimrc
fi

if [ -x $HOME/dotfiles/.gvimrc ]
then
    rm -f $HOME/.gvimrc
    cp $HOME/dotfiles/.gvimrc $HOME/.gvimrc
fi

if [ -x $HOME/dotfiles/.screenrc ]
then
    rm -f $HOME/.screenrc
    ln -s $HOME/dotfiles/.screenrc $HOME/.screenrc
fi

if [ -x $HOME/dotfiles/.tmux.conf ]
then
    rm -f $HOME/.tmux.conf
    ln -s $HOME/dotfiles/.tmux.conf $HOME/.tmux.conf
fi

if [ -x $HOME/dotfiles/.minttyrc ]
then
    rm -f $HOME/.minttyrc
    ln -s $HOME/dotfiles/.minttyrc $HOME/.minttyrc
fi

if [ -x $HOME/dotfiles/.dircolors ]
then
    rm -f $HOME/.dircolors
    ln -s $HOME/dotfiles/.dircolors $HOME/.dircolors
fi

